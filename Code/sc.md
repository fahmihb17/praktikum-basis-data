```sql
CREATE TABLE `player` (
  `id_player` int PRIMARY KEY,
  `username` varchar(255),
  `total_match` int,
  `favorit_hero` varchar(255),
  `id_match` int,
  `id_tier` int,
  `id_squad` int,
  `id_skin` int,
  `id_events` int
);

CREATE TABLE `Hero` (
  `id_hero` int PRIMARY KEY,
  `hero_name` varchar(255),
  `id_role` int,
  `price` int,
  `difficulty` int,
  `id_emblem` int,
  `id_item` int
);

CREATE TABLE `Skin` (
  `id_skin` int PRIMARY KEY,
  `skin_name` varchar(255),
  `id_hero` int,
  `price` varchar(255)
);

CREATE TABLE `item` (
  `id_item` int PRIMARY KEY,
  `item_name` varchar(255),
  `item_type` varchar(255),
  `price` int
);

CREATE TABLE `matching` (
  `id_match` int PRIMARY KEY,
  `id_map` int,
  `duration` DATETIME,
  `id_player` int,
  `id_hero` int,
  `id_skin` int,
  `id_emblem` int,
  `id_minion` int,
  `id_tower` int,
  `id_jungle_monster` int
);

CREATE TABLE `emblem` (
  `id_emblem` int PRIMARY KEY,
  `emblem_name` varchar(255),
  `emblem_type_id` int,
  `level` int
);

CREATE TABLE `squad` (
  `id_squad` int PRIMARY KEY,
  `squad_name` varchar(255),
  `id_player` int
);

CREATE TABLE `map` (
  `id_map` int PRIMARY KEY,
  `map_name` varchar(255)
);

CREATE TABLE `tier` (
  `id_tier` int PRIMARY KEY,
  `name_tier` varchar(255),
  `id_player` int
);

CREATE TABLE `role` (
  `id_role` int PRIMARY KEY,
  `name_role` varchar(255),
  `id_hero` int
);

CREATE TABLE `group` (
  `id_grup` int PRIMARY KEY,
  `nama_group` varchar(255),
  `id_player` int
);

CREATE TABLE `Minion` (
  `id_minion` int PRIMARY KEY,
  `name_minion` varchar(255)
);

CREATE TABLE `tower` (
  `id_tower` int PRIMARY KEY,
  `name_tower` varchar(255)
);

CREATE TABLE `jungle_monster` (
  `id_jungle_monster` int PRIMARY KEY,
  `name_jungle` varchar(255)
);

CREATE TABLE `game_mode` (
  `id_game_mode` int PRIMARY KEY,
  `name_game_mode` varchar(255)
);

CREATE TABLE `Top_up` (
  `id_top_up` int PRIMARY KEY,
  `id_player` int
);

CREATE TABLE `leaderboard` (
  `id_leaderboard` int PRIMARY KEY,
  `id_player` int
);

CREATE TABLE `achievements` (
  `id_achievements` int PRIMARY KEY,
  `id_player` int
);

CREATE TABLE `battle_points` (
  `id_battle_points` int PRIMARY KEY,
  `id_player` int
);

CREATE TABLE `diamonds` (
  `id_top_up` int,
  `id_player` int
);

CREATE TABLE `tipe_akun` (
  `id_tipe` int[PK],
  `id_player` int
);

CREATE TABLE `Mailbox` (
  `id_player` int
);

CREATE TABLE `events` (
  `id_events` int PRIMARY KEY,
  `name_events` int,
  `id_player` int
);

CREATE TABLE `statistik` (
  `id_player` int,
  `id_match` int
);

CREATE TABLE `battle_spells` (
  `id_battle_spells` int PRIMARY KEY,
  `id_hero` int,
  `name_spells` varchar(255)
);

ALTER TABLE `squad` ADD FOREIGN KEY (`id_squad`) REFERENCES `player` (`id_squad`);

ALTER TABLE `tier` ADD FOREIGN KEY (`id_tier`) REFERENCES `player` (`id_tier`);

ALTER TABLE `player` ADD FOREIGN KEY (`id_player`) REFERENCES `matching` (`id_player`);

ALTER TABLE `emblem` ADD FOREIGN KEY (`id_emblem`) REFERENCES `Hero` (`id_emblem`);

ALTER TABLE `player` ADD FOREIGN KEY (`id_player`) REFERENCES `tier` (`id_player`);

ALTER TABLE `map` ADD FOREIGN KEY (`id_map`) REFERENCES `player` (`id_match`);

ALTER TABLE `Hero` ADD FOREIGN KEY (`id_hero`) REFERENCES `Skin` (`id_hero`);

ALTER TABLE `Hero` ADD FOREIGN KEY (`id_hero`) REFERENCES `matching` (`id_hero`);

ALTER TABLE `Skin` ADD FOREIGN KEY (`id_skin`) REFERENCES `matching` (`id_skin`);

ALTER TABLE `role` ADD FOREIGN KEY (`id_role`) REFERENCES `Hero` (`id_role`);

ALTER TABLE `emblem` ADD FOREIGN KEY (`id_emblem`) REFERENCES `matching` (`id_emblem`);

ALTER TABLE `player` ADD FOREIGN KEY (`id_player`) REFERENCES `squad` (`id_player`);

ALTER TABLE `item` ADD FOREIGN KEY (`id_item`) REFERENCES `Hero` (`id_item`);

ALTER TABLE `jungle_monster` ADD FOREIGN KEY (`id_jungle_monster`) REFERENCES `matching` (`id_jungle_monster`);

ALTER TABLE `battle_points` ADD FOREIGN KEY (`id_player`) REFERENCES `player` (`id_player`);

ALTER TABLE `diamonds` ADD FOREIGN KEY (`id_player`) REFERENCES `player` (`id_player`);

ALTER TABLE `statistik` ADD FOREIGN KEY (`id_player`) REFERENCES `player` (`id_player`);

ALTER TABLE `leaderboard` ADD FOREIGN KEY (`id_player`) REFERENCES `player` (`id_player`);

ALTER TABLE `achievements` ADD FOREIGN KEY (`id_player`) REFERENCES `player` (`id_player`);

ALTER TABLE `tipe_akun` ADD FOREIGN KEY (`id_player`) REFERENCES `player` (`id_player`);

ALTER TABLE `Mailbox` ADD FOREIGN KEY (`id_player`) REFERENCES `player` (`id_player`);

ALTER TABLE `Top_up` ADD FOREIGN KEY (`id_player`) REFERENCES `player` (`id_player`);

ALTER TABLE `events` ADD FOREIGN KEY (`id_player`) REFERENCES `player` (`id_player`);

ALTER TABLE `group` ADD FOREIGN KEY (`id_player`) REFERENCES `player` (`id_player`);

ALTER TABLE `map` ADD FOREIGN KEY (`id_map`) REFERENCES `matching` (`id_map`);

ALTER TABLE `Minion` ADD FOREIGN KEY (`id_minion`) REFERENCES `matching` (`id_minion`);

ALTER TABLE `tower` ADD FOREIGN KEY (`id_tower`) REFERENCES `matching` (`id_tower`);

ALTER TABLE `Hero` ADD FOREIGN KEY (`id_hero`) REFERENCES `role` (`id_hero`);

ALTER TABLE `Hero` ADD FOREIGN KEY (`id_hero`) REFERENCES `battle_spells` (`id_hero`);

ALTER TABLE `matching` ADD FOREIGN KEY (`id_match`) REFERENCES `statistik` (`id_match`);


```
