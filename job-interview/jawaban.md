# No 1

![](https://gitlab.com/fahmihb17/praktikum-basis-data/-/raw/main/job-interview/Documents/Screenshot__6_.png)

# No 2
```sql
Table player {
id_player int [primary key]
username varchar
total_match int
favorit_hero varchar
id_match int
id_tier int
id_squad int
id_skin int
id_events int

}

Table Hero {
id_hero int [primary key]
hero_name varchar
id_role int
price int
difficulty int
id_emblem int
id_item int
}

Table Skin {
id_skin int [PK]
skin_name varchar
id_hero int
price varchar

}

Table item {
id_item int [primary key]
item_name varchar
item_type varchar
price int
}


Table matching {
id_match int [primary key]
id_map int
duration DATETIME
id_player int
id_hero int
id_skin int
id_emblem int
id_minion int
id_tower int
id_jungle_monster int

}


Table emblem {
id_emblem int [primary key]
emblem_name varchar
emblem_type_id int
level int

}

Table squad {
id_squad int [primary key]
squad_name varchar
id_player int

}


Table map {
id_map int [primary key]
map_name varchar
}


Table tier {
id_tier int [primary key]
name_tier varchar
id_player int
}



Table role {
  id_role int [PK]
  name_role varchar
  id_hero int
}

Table group{
  id_grup int [pk]
  nama_group varchar
  id_player int
}

Table Minion {
  id_minion int [PK]
  name_minion varchar
}

Table tower {
  id_tower int [PK]
  name_tower varchar
}

Table jungle_monster{
  id_jungle_monster int [PK]
  name_jungle varchar
}

Table game_mode{
  id_game_mode int [PK]
  name_game_mode varchar
}

Table Top_up{
  id_top_up int [PK]
  id_player int
}

Table leaderboard{
  id_leaderboard int [PK]
  id_player int 
}

Table achievements{
  id_achievements int [PK]
  id_player int
}

Table battle_points{
  id_battle_points int [PK]
  id_player int
}

Table diamonds {
  id_top_up int 
  id_player int

}
Table tipe_akun{
  id_tipe int[PK]
  id_player int
}
Table Mailbox{
  id_player int
}
Table events{
  id_events int [PK]
  name_events int
  id_player int
}
Table statistik{
  id_player int 
  id_match int
}
Table battle_spells{
  id_battle_spells int [PK]
  id_hero int 
  name_spells varchar 
}


Ref {
  player.id_squad < squad.id_squad
}

Ref {
  player.id_tier < tier.id_tier
}

Ref{
  matching.id_player < player.id_player
}
Ref{
  Hero.id_emblem < emblem.id_emblem
}
Ref{
  tier.id_player < player.id_player
}
Ref{
  player.id_match < map.id_map
}
Ref{
  Skin.id_hero < Hero.id_hero
}
Ref{
  matching.id_hero < Hero.id_hero  
}
ref{
  matching.id_skin < Skin.id_skin
}
ref{
  Hero.id_role < role.id_role
}

Ref{ matching.id_emblem < emblem.id_emblem
}
Ref{ squad.id_player < player.id_player
}


Ref{ Hero.id_item < item.id_item
}
ref{
  matching.id_jungle_monster < jungle_monster.id_jungle_monster
}
ref {
  player.id_player < battle_points.id_player
}
ref{
  player.id_player < diamonds.id_player
}

ref{
  player.id_player < statistik.id_player
}
ref{
  player.id_player < leaderboard.id_player
}
ref {
  player.id_player < achievements.id_player
}
ref{
  player.id_player < tipe_akun.id_player
}
ref{
  player.id_player < Mailbox.id_player
}
ref{
  player.id_player < Top_up.id_player
}
ref{
  player.id_player < events.id_player
}
ref {
  player.id_player < group.id_player
}
ref {
  matching.id_map < map.id_map
}
ref {
  matching.id_minion < Minion.id_minion
}
ref{
  matching.id_tower < tower.id_tower
}
ref {
  role.id_hero < Hero.id_hero
}
ref {
  battle_spells.id_hero < Hero.id_hero
}
ref{
  statistik.id_match < matching.id_match
}

```

# No 3

## Create
```sql
CREATE TABLE `tier_player` (
  `id_tier` varchar(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name_tier` varchar(255) DEFAULT NULL
):
```

## Update
```sql
update Hero 
set difficulty = 'Normal'
WHERE id_hero = 1;
```

## Read
```sql
SELECT * from Hero WHERE price = 15000;
```

## Delete
```sql
DELETE FROM Hero 
WHERE difficulty = 'Easy';
```
## Insert
```sql
INSERT INTO role(id_role, name_role)
VALUES 
(1, 'Assasin'),
(2, 'Tank'),
(3, 'Support'),
(4, 'Marksman'),
(5, 'Mage'),
(6, 'Fighter')
```


# No 4
## 5 Analisis Pertanyaan
```sql
-- 1. Berapa Jumlah hero mobile legends diurutkan berdasarkan tingkat kesulitan?

SELECT 
COUNT(*) AS total_hero,
difficulty
FROM Hero
GROUP BY difficulty 
order by total_hero DESC
```
```sql
--2. Berapa jumlah hero berdasarkan tingkat kesulitan dan price, diurutkan berdasarkan tingkat kesulitan dan price?

select 
difficulty,
price,
count(*) as jumlah_hero
FROM Hero
GROUP BY difficulty, price
ORDER By difficulty DESC
```
```sql
--3. Jumlah Hero terbanyak berdasarkan role
SELECT
	CASE 
        WHEN id_role = 1 THEN 'Assasin'
        WHEN id_role = 2 THEN 'Tank'
        WHEN id_role = 3 THEN 'Support'
        WHEN id_role = 4 THEN 'Marksman'
        WHEN id_role = 5 THEN 'Mage'
        WHEN id_role = 6 THEN 'Fighter'
	ELSE 'Tidak diketahui' END as Role_hero,
    COUNT(*) AS jumlah_Hero
FROM Hero
GROUP BY id_role
ORDER BY COUNT(*) DESC
```

```sql
--4.	Berapa banyak hero sih dirole marksman

select count(*) as jumlah_Hero
from Hero 
WHERE id_role = (SELECT id_role from role WHERE name_role = 'Marksman');
```

```sql
--5.	Urutkan tier berdasarkan jumlah pemain terbanyak?

select 
kk.name_tier AS Rank,
COUNT (*) as jumlah_pemain
from tier_player kk
left join player p
on p.id_tier = kk.id_tier
group by kk.id_tier
order by jumlah_pemain DESC
```


